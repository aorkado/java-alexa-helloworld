package com.aorkado.helloworld.requesthandlers.matchers;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.aorkado.helloworld.AbstractTest;
import com.aorkado.helloworld.Constants;
import com.aorkado.helloworld.utils.PredicatesUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class StopIntentMatcherTest extends AbstractTest {

  private PredicatesUtils predicatesUtils;
  private StopIntentMatcher matcher;

  @Before
  public void before() {
    predicatesUtils = mock(PredicatesUtils.class);

    matcher = new StopIntentMatcher();
    matcher.setPredicatesUtils(predicatesUtils);
  }

  @Test
  public void match() {
    HandlerInput handlerInput = mock(HandlerInput.class);
    doReturn(true).when(handlerInput).matches(any());

    boolean result = matcher.match(handlerInput);
    Assert.assertTrue(result);

    verify(predicatesUtils).intentName(Constants.STOP_INTENT);
    verifyNoMoreInteractions(predicatesUtils);
  }
}

package com.aorkado.helloworld.requesthandlers.models;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;
import com.aorkado.helloworld.AbstractTest;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FallbackIntentHandlerModelTest extends AbstractTest {

  private FallbackIntentHandlerModel handlerModel;

  @Before
  public void before() {
    handlerModel = new FallbackIntentHandlerModel();
  }

  @Test
  public void handle() {
    Optional optional = Optional.of(String.class);

    ResponseBuilder responseBuilder = mock(ResponseBuilder.class);
    doReturn(optional).when(responseBuilder).build();

    HandlerInput handlerInput = mock(HandlerInput.class);
    doReturn(responseBuilder).when(handlerInput).getResponseBuilder();

    Optional<Response> result = handlerModel.handle(handlerInput);

    Assert.assertSame(optional, result);

    verify(responseBuilder).withSpeech("Sorry, I don't know that. You can say try saying help!");
    verify(responseBuilder)
        .withSimpleCard("HelloWorld", "Sorry, I don't know that. You can say try saying help!");
    verify(responseBuilder).withReprompt("Sorry, I don't know that. You can say try saying help!");
    verify(responseBuilder).build();
    verifyNoMoreInteractions(responseBuilder);
  }
}

package com.aorkado.helloworld.tools.builders;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.amazon.ask.Skill;
import com.amazon.ask.builder.StandardSkillBuilder;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.handler.GenericRequestHandler;
import com.aorkado.helloworld.AbstractTest;
import com.aorkado.helloworld.tools.SystemEnvReader;
import com.aorkado.helloworld.utils.SkillsUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SkillBuilderTest extends AbstractTest {

  private static final String SKILL_ID = "1234567890";

  private Skill skill;
  private StandardSkillBuilder standardSkillBuilder;
  private SkillsUtils skillsUtils;
  private SystemEnvReader systemEnvReader;
  private List<GenericRequestHandler<HandlerInput, Optional<Response>>> requestHandlerList;
  private RequestHandlerListBuilder requestHandlerListBuilder;
  private SkillBuilder builder;

  @Before
  public void before() {
    skill = mock(Skill.class);
    standardSkillBuilder = mock(StandardSkillBuilder.class);
    doReturn(skill).when(standardSkillBuilder).build();

    skillsUtils = mock(SkillsUtils.class);
    doReturn(standardSkillBuilder).when(skillsUtils).standard();

    systemEnvReader = mock(SystemEnvReader.class);
    doReturn(SKILL_ID).when(systemEnvReader).skillId();

    requestHandlerList = mock(ArrayList.class);
    requestHandlerListBuilder = mock(RequestHandlerListBuilder.class);
    doReturn(requestHandlerList).when(requestHandlerListBuilder).build();

    builder = new SkillBuilder();
    builder.setRequestHandlerListBuilder(requestHandlerListBuilder);
    builder.setSkillsUtils(skillsUtils);
    builder.setSystemEnvReader(systemEnvReader);
  }

  @Test
  public void build() {
    Skill result = builder.build();

    Assert.assertSame(skill, result);

    verify(skillsUtils).standard();
    verifyNoMoreInteractions(skillsUtils);

    verify(systemEnvReader).skillId();
    verifyNoMoreInteractions(systemEnvReader);

    verify(requestHandlerListBuilder).build();
    verifyNoMoreInteractions(requestHandlerListBuilder);

    verify(standardSkillBuilder).withSkillId(SKILL_ID);
    verify(standardSkillBuilder).addRequestHandlers(requestHandlerList);
    verify(standardSkillBuilder).build();
    verifyNoMoreInteractions(standardSkillBuilder);
  }
}

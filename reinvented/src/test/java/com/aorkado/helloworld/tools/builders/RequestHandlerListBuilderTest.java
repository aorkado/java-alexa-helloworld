package com.aorkado.helloworld.tools.builders;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.handler.GenericRequestHandler;
import com.aorkado.helloworld.AbstractTest;
import com.aorkado.helloworld.pojos.BaseIntentHandler;
import com.aorkado.helloworld.requesthandlers.matchers.CancelIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.FallbackIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.HelloWorldIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.HelpIntentMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.LaunchRequestMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.SessionEndedRequestMatcher;
import com.aorkado.helloworld.requesthandlers.matchers.StopIntentMatcher;
import com.aorkado.helloworld.requesthandlers.models.CancelIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.FallbackIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.HelloWorldIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.HelpIntentHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.LaunchRequestHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.SessionEndedRequestHandlerModel;
import com.aorkado.helloworld.requesthandlers.models.StopIntentHandlerModel;
import java.util.List;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class RequestHandlerListBuilderTest extends AbstractTest {

  private IntentHandlerBuilder intentHandlerBuilder;
  private RequestHandlerListBuilder builder;

  @Before
  public void before() {
    BaseIntentHandler mockedIntentHandler = mock(BaseIntentHandler.class);
    intentHandlerBuilder = mock(IntentHandlerBuilder.class);
    doReturn(mockedIntentHandler).when(intentHandlerBuilder).build(any(), any());

    builder = new RequestHandlerListBuilder();
    builder.setIntentHandlerBuilder(intentHandlerBuilder);
  }

  @Test
  public void build() {
    List<GenericRequestHandler<HandlerInput, Optional<Response>>> result = builder.build();

    Assert.assertEquals(7, result.size());

    verify(intentHandlerBuilder).build(
        LaunchRequestMatcher.class,
        LaunchRequestHandlerModel.class
    );
    verify(intentHandlerBuilder).build(
        SessionEndedRequestMatcher.class,
        SessionEndedRequestHandlerModel.class
    );
    verify(intentHandlerBuilder).build(
        HelpIntentMatcher.class,
        HelpIntentHandlerModel.class
    );
    verify(intentHandlerBuilder).build(
        CancelIntentMatcher.class,
        CancelIntentHandlerModel.class
    );
    verify(intentHandlerBuilder).build(
        StopIntentMatcher.class,
        StopIntentHandlerModel.class
    );
    verify(intentHandlerBuilder).build(
        FallbackIntentMatcher.class,
        FallbackIntentHandlerModel.class
    );
    verify(intentHandlerBuilder).build(
        HelloWorldIntentMatcher.class,
        HelloWorldIntentHandlerModel.class
    );
    verifyNoMoreInteractions(intentHandlerBuilder);
  }
}

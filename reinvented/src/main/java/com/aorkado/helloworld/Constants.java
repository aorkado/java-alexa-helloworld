package com.aorkado.helloworld;

public class Constants {

  public static final String CANCEL_INTENT = "AMAZON.CancelIntent";
  public static final String STOP_INTENT = "AMAZON.StopIntent";
  public static final String HELP_INTENT = "AMAZON.HelpIntent";
  public static final String FALLBACK_INTENT = "AMAZON.FallbackIntent";
  public static final String HELLO_WORLD_INTENT = "HelloWorldIntent";
}

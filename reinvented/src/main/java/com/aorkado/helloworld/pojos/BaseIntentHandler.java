package com.aorkado.helloworld.pojos;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.Response;
import com.aorkado.helloworld.requesthandlers.matchers.Matcher;
import com.aorkado.helloworld.requesthandlers.models.HandlerModel;
import java.util.Optional;

public class BaseIntentHandler implements RequestHandler {

  private Matcher matcher;
  private HandlerModel handlerModel;

  public void setMatcher(Matcher matcher) {
    this.matcher = matcher;
  }

  public void setHandlerModel(HandlerModel handlerModel) {
    this.handlerModel = handlerModel;
  }

  private BaseIntentHandler() {
  }

  public static BaseIntentHandler build(Matcher matcher, HandlerModel handlerModel) {
    BaseIntentHandler intentHandler = new BaseIntentHandler();
    intentHandler.setMatcher(matcher);
    intentHandler.setHandlerModel(handlerModel);
    return intentHandler;
  }

  @Override
  public boolean canHandle(HandlerInput handlerInput) {
    return matcher.match(handlerInput);
  }

  @Override
  public Optional<Response> handle(HandlerInput handlerInput) {
    return handlerModel.handle(handlerInput);
  }
}
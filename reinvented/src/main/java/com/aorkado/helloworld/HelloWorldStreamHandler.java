package com.aorkado.helloworld;

import com.amazon.ask.AlexaSkill;
import com.amazon.ask.SkillStreamHandler;
import com.aorkado.helloworld.tools.builders.SkillBuilder;
import jodd.exception.ExceptionUtil;
import jodd.petite.AutomagicPetiteConfigurator;
import jodd.petite.PetiteContainer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorldStreamHandler extends SkillStreamHandler {

  private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldStreamHandler.class);

  private static PetiteContainer petiteContainer = null;

  public HelloWorldStreamHandler() {
    super(getSkill());
  }

  private static AlexaSkill getSkill() {
    try {
      if (null == petiteContainer) {
        initPetiteContainer();
      }
      return petiteContainer.getBean(SkillBuilder.class).build();
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
      throw e;
    }
  }

  private static void initPetiteContainer() {
    petiteContainer = new PetiteContainer();
    petiteContainer.addSelf();

    AutomagicPetiteConfigurator configurator = new AutomagicPetiteConfigurator(petiteContainer);
    configurator.configure();
  }
}
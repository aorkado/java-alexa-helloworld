package com.aorkado.helloworld.tools.builders;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.request.handler.GenericRequestHandler;
import com.aorkado.helloworld.pojos.BaseIntentHandler;
import com.aorkado.helloworld.requesthandlers.matchers.Matcher;
import com.aorkado.helloworld.requesthandlers.models.HandlerModel;
import java.util.Optional;
import jodd.petite.PetiteContainer;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

@PetiteBean
public class IntentHandlerBuilder {

  private PetiteContainer petiteContainer;

  @PetiteInject
  public void setPetiteContainer(PetiteContainer petiteContainer) {
    this.petiteContainer = petiteContainer;
  }

  public GenericRequestHandler<HandlerInput, Optional<Response>> build(
      Class<? extends Matcher> matcherClass,
      Class<? extends HandlerModel> handlerModelClass
  ) {
    Matcher matcher = petiteContainer.getBean(matcherClass);
    HandlerModel handlerModel = petiteContainer.getBean(handlerModelClass);
    return BaseIntentHandler.build(matcher, handlerModel);
  }
}

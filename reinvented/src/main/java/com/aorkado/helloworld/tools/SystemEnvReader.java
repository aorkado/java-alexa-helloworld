package com.aorkado.helloworld.tools;

import com.aorkado.helloworld.utils.SystemUtils;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;
import jodd.util.StringPool;

@PetiteBean
public class SystemEnvReader {

  public enum SystemEnvKeys {
    SKILL_ID,
  }

  private SystemUtils systemUtils;

  @PetiteInject
  public void setSystemUtils(SystemUtils systemUtils) {
    this.systemUtils = systemUtils;
  }

  public String skillId() {
    return systemUtils.getEnvironmentVariable(SystemEnvKeys.SKILL_ID.name(), StringPool.EMPTY);
  }
}

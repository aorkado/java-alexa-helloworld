package com.aorkado.helloworld.utils;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Request;
import com.amazon.ask.request.Predicates;
import java.util.function.Predicate;
import jodd.petite.meta.PetiteBean;

@PetiteBean
public class PredicatesUtils {

  public Predicate<HandlerInput> intentName(String intentName) {
    return Predicates.intentName(intentName);
  }

  public Predicate<HandlerInput> requestType(Class<? extends Request> requestType) {
    return Predicates.requestType(requestType);
  }
}

package com.aorkado.helloworld.requesthandlers.models;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;
import java.util.Optional;
import jodd.petite.meta.PetiteBean;

@PetiteBean
public class CancelIntentHandlerModel extends AbstractHandlerModel {

  public Optional<Response> handle(HandlerInput handlerInput) {
    String speechText = "Goodbye";

    ResponseBuilder responseBuilder = handlerInput.getResponseBuilder();
    responseBuilder.withSpeech(speechText);
    responseBuilder.withSimpleCard("HelloWorld", speechText);
    return responseBuilder.build();
  }
}
package com.aorkado.helloworld.requesthandlers.models;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;
import java.util.Optional;
import jodd.petite.meta.PetiteBean;

@PetiteBean
public class FallbackIntentHandlerModel extends AbstractHandlerModel {

  public Optional<Response> handle(HandlerInput handlerInput) {
    String speechText = "Sorry, I don't know that. You can say try saying help!";

    ResponseBuilder responseBuilder = handlerInput.getResponseBuilder();
    responseBuilder.withSpeech(speechText);
    responseBuilder.withSimpleCard("HelloWorld", speechText);
    responseBuilder.withReprompt(speechText);
    return responseBuilder.build();
  }
}
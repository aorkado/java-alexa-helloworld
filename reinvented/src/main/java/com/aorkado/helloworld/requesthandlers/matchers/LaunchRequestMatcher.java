package com.aorkado.helloworld.requesthandlers.matchers;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.LaunchRequest;
import jodd.exception.ExceptionUtil;
import jodd.petite.meta.PetiteBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PetiteBean
public class LaunchRequestMatcher extends AbstractMatcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(LaunchRequestMatcher.class);

  public boolean match(HandlerInput handlerInput) {
    try {
      return handlerInput.matches(predicatesUtils.requestType(LaunchRequest.class));
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
      return false;
    }
  }
}

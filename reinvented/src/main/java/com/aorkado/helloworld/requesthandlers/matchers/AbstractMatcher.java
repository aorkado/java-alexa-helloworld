package com.aorkado.helloworld.requesthandlers.matchers;


import com.aorkado.helloworld.utils.PredicatesUtils;
import jodd.petite.meta.PetiteBean;
import jodd.petite.meta.PetiteInject;

@PetiteBean
public abstract class AbstractMatcher implements Matcher {

  protected PredicatesUtils predicatesUtils;

  @PetiteInject
  public void setPredicatesUtils(PredicatesUtils predicatesUtils) {
    this.predicatesUtils = predicatesUtils;
  }
}
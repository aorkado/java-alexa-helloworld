package com.aorkado.helloworld.requesthandlers.models;

import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.model.Response;
import com.amazon.ask.response.ResponseBuilder;
import java.util.Optional;
import jodd.petite.meta.PetiteBean;

@PetiteBean
public class SessionEndedRequestHandlerModel extends AbstractHandlerModel {

  public Optional<Response> handle(HandlerInput handlerInput) {
    ResponseBuilder responseBuilder = handlerInput.getResponseBuilder();
    return responseBuilder.build();
  }
}
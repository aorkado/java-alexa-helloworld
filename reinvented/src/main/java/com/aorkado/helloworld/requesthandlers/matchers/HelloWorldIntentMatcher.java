package com.aorkado.helloworld.requesthandlers.matchers;


import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.aorkado.helloworld.Constants;
import jodd.exception.ExceptionUtil;
import jodd.petite.meta.PetiteBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@PetiteBean
public class HelloWorldIntentMatcher extends AbstractMatcher {

  private static final Logger LOGGER = LoggerFactory.getLogger(HelloWorldIntentMatcher.class);

  public boolean match(HandlerInput handlerInput) {
    try {
      return handlerInput.matches(predicatesUtils.intentName(Constants.HELLO_WORLD_INTENT));
    } catch (Exception e) {
      LOGGER.error(ExceptionUtil.exceptionStackTraceToString(e));
      return false;
    }
  }
}
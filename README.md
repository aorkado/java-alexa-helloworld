# HelloWorld Reinvented

A worked-out version of the official helloworld sample from alexa-java repository.

The reinvented version contains a refactoring of the original classes into components for testing, maintainability and re-usability

## Features:
 - log4j2 as logging framework.
 - dependency injection using Jodd Petite micro-framework.
 - system env. variables reader for aws lambda
 - tests for all logic components except wrappers (utils folder).
 
## Project Folder Structure:
 - pojos => containes POJOs classes
 - requesthandlers 
   + matchers => contains matcher classes with the intent-handler matching logic.
   + models => contains handlerModel classes, with the intent-handler handling logic.
 - tools => with helper classes such builders, for example.
 - utils => contains util classes that wrap 3rd party classes, for testing purposes.
 
## Notes:
- HelloWorldStreamHandler is the "main" class to be called by AWS lambda. It loads the "petiteContainer" (which is the IOC container) only once, to avoid warm-up times.
- There is no IntentHandler classes per-se. The RequestHandler implementations on the original sample project require the classes to handle to different functionalities, matching and handling. For that reason I prefer to split the functionality into components (matchers and handlemodels)
- The IntentHandlerBuilder is the component in charge of combining matchers and handlermodels and create the intenthandlers for the skill.
- The SkillBuilder is the component in charge of creating the skill to pass to the HelloWorldSteramHandler and initialize the ASK SDK code behind.

## Deployment in AWS Lambda
- Remember to add a system env. variable named SKILL_ID with your skill id. This id can be found on your Alexa Development Console.

## For your consideration
- Size of fat-jar is about 12MB
- Runtimes taken from Cloudwatch logs:
    + Duration: 1338.50 ms | Billed Duration: 1400 ms | Memory Size: 512 MB | Max Memory Used: 174 MB => This is the first run (including warm-up)
   + Duration: 68.27 ms | Billed Duration: 100 ms | Memory Size: 512 MB | Max Memory Used: 174 MB => This is a second run (no warm-up needed)
   + Coffee time so that lambda machine gets destroyed :)
   + Duration: 1500.62 ms | Billed Duration: 1600 ms | Memory Size: 512 MB | Max Memory Used: 174 MB => This is again a first run (including warm-up)
   + Duration: 65.97 ms | Billed Duration: 100 ms | Memory Size: 512 MB | Max Memory Used: 174 MB => This is again a second run (no warm-up needed)
   

## Contact
For any questions or comments, you can contact me via linkedin => https://www.linkedin.com/in/javierochoaorihuel/


